using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApplication1.Data;
using WebApplication1.Services;
using Xunit;

namespace Tests
{
    public class UnitTest1
    {
        public string Value { get; set; } = "Test";

        [Fact]
        public async Task Test1()
        {
            var mockListItemsCollection = new Moq.Mock<DbSet<ListItem>>();
            mockListItemsCollection.Setup(x => x.FindAsync(Moq.It.IsAny<Guid>()))
                .Returns(Task.FromResult(
                    new ListItem { CreatedAt = DateTimeOffset.Now.AddDays(-31) }
                ));

            var dbcontextMock = new Moq.Mock<TestContext>(new DbContextOptions<TestContext>());
            dbcontextMock.SetupGet(x => x.ListItems)
                .Returns(mockListItemsCollection.Object);

            var mock = new Moq.Mock<ListItemsService>(dbcontextMock.Object);

            Exception ex = await Assert.ThrowsAsync<InvalidOperationException>(async () => await mock.Object.DeleteItem(Guid.NewGuid()));
            Assert.Equal("Cannot delete items older than 30 days", ex.Message);
        }

        [Fact]
        public void Test2()
        {
            var item = new ListItem { CreatedAt = DateTimeOffset.Now.AddDays(-31) };

            var svc = new ListItemsService(null);

            Exception ex = Assert.Throws<InvalidOperationException>(() => svc.ValidateIfItemCanBeDeleted(item));
            Assert.Equal("Cannot delete items older than 30 days", ex.Message);
        }

        [Fact]
        public void Test3()
        {
            var item = new ListItem
            {
                CreatedAt = DateTimeOffset.Now.AddDays(-1)
            };

            var svc = new ListItemsService(null);
            svc.ValidateIfItemCanBeDeleted(item);

            Assert.True(true);
        }

        [Fact(DisplayName = "foo")]
        public void Reflection()
        {
            var type = typeof(UnitTest1);

            var test1Attributes = type.GetMethod(nameof(UnitTest1.Test1)).GetCustomAttributes(true);

            var propertyName = "Value";

            Func<UnitTest1, string> func = x => x.Value;

            var value = func(this);

            var arg = Expression.Parameter(type, "x");
            var body = Expression.Property(arg, propertyName);
            var lambda = Expression.Lambda<Func<UnitTest1, string>>(body, arg);
            var dynamicallyCreatedFunc = lambda.Compile();
            var value2 = dynamicallyCreatedFunc(this);

            Assert.Equal(value, value2);
        }

        [Fact]
        public void CreateInstanceViaReflection()
        {
            var fooTypeInfo = typeof(Foo);
            var fooInstance = (Foo)Activator.CreateInstance(fooTypeInfo, "bar");
            Assert.Equal("bar", fooInstance.Val);
        }

        public class Foo
        {
            public readonly string Val;
            public Foo(string val)
            {
                Val = val;
            }
        }
    }
}

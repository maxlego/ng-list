﻿namespace WebApplication1.Data.Enums
{
    public enum ListItemStatus
    {
        Pending = 1,
        Done = 2,
    }
}

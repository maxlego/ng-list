﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Data
{
    public static class DbSetExtensions
    {
        public static void RemoveById<TEntity, TId>(this DbSet<TEntity> dbSet, TId id) 
            where TEntity : class, IEntityWithTypedId<TId>, new()
        {
            var entity = dbSet.Local.FirstOrDefault(x => x.Id.Equals(id));
            if (entity == null)
            {
                entity = new TEntity { Id = id };
                dbSet.Attach(entity);
            }

            dbSet.Remove(entity);
        }
    }

    public static class QueryableExtensions
    {
        private static Lazy<MethodInfo> OrderByMethodInfo = new Lazy<MethodInfo>(GetOrderByMehodInfo);
        private static Lazy<MethodInfo> OrderByDescendingMethodInfo = new Lazy<MethodInfo>(GetOrderByDescendingMehodInfo);

        private static MethodInfo GetOrderByDescendingMehodInfo()
        {
            return typeof(Queryable).GetMethods()
                .Where(x => x.Name == nameof(Queryable.OrderByDescending) && x.GetParameters().Length == 2)
                .FirstOrDefault();
        }
        private static MethodInfo GetOrderByMehodInfo()
        {
            return typeof(Queryable).GetMethods()
                .Where(x => x.Name == nameof(Queryable.OrderBy) && x.GetParameters().Length == 2)
                .FirstOrDefault();
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> q, string propertyName, bool desc)
        {
            var type = typeof(T);
            var prop = type.GetProperty(propertyName);
            var parameterExpression = Expression.Parameter(type, "x");
            var body = Expression.Property(parameterExpression, propertyName);
            var lambda = Expression.Lambda(body, parameterExpression);

            if (desc)
            {
                return (IQueryable<T>)OrderByDescendingMethodInfo.Value
                    .MakeGenericMethod(type, prop.PropertyType)
                    .Invoke(null, new object[] { q, lambda });
            }

            return (IQueryable<T>)OrderByMethodInfo.Value
                .MakeGenericMethod(type, prop.PropertyType)
                .Invoke(null, new object[] { q, lambda });
        }
    }
}

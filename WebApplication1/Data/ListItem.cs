﻿using System;
using System.ComponentModel.DataAnnotations;
using WebApplication1.Data.Enums;

namespace WebApplication1.Data
{
    public class ListItem : IEntityWithTypedId<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public ListItemStatus Status { get; set; }

        public ListItemData Data { get; set; }
    }

    public class ListItemData
    {
        public string Foo { get; set; }
        public string Bar { get; set; }
    }
}

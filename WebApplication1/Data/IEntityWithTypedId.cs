﻿namespace WebApplication1.Data
{
    public interface IEntityWithTypedId<T>
    {
        T Id { get; set; }
    }
}

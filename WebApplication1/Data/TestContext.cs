﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace WebApplication1.Data
{
    public class TestContext : DbContext
    {
        public virtual DbSet<ListItem> ListItems { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public TestContext(DbContextOptions<TestContext> options) : base(options)
        {
            this.Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ListItem>().ToTable("X");

            modelBuilder.Entity<ListItem>()
                .Property(x => x.Name)
                .HasMaxLength(500);

            modelBuilder.Entity<ListItem>()
                .Property(x => x.Data)
                .HasJsonConversion();
        }
    }

    public static class PropertyBuilderExt
    {
        public static PropertyBuilder<TProperty> HasJsonConversion<TProperty>(this PropertyBuilder<TProperty> builder)
        {
            return builder.HasConversion(
                x => JsonConvert.SerializeObject(x),
                x => JsonConvert.DeserializeObject<TProperty>(x)
            );
        }
    }
}

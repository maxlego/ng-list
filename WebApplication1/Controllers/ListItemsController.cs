﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApplication1.Data;
using WebApplication1.Data.Enums;
using WebApplication1.Dto;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    public class ListItemsController : ApiControllerBase
    {
        private readonly TestContext _context;
        private readonly IListItemsService _listItemsService;
        private const int DEFAULT_PAGE_SIZE = 20;

        public ListItemsController(
            TestContext context, 
            IListItemsService listItemsService,
            ILogger<ListItemsController> logger)
        {
            _context = context;
            _listItemsService = listItemsService;
            logger.LogDebug("List items controller created");
        }

        [HttpGet]
        public Task<IActionResult> Index()
        {
            return Index(null);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetItem(Guid id)
        {
            var item = await _context.ListItems.FindAsync(id);
            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> Index([FromBody] FilterInfo filterInfo)
        {
            IQueryable<ListItem> seed = _context.ListItems;

            if (filterInfo != null)
            {
                if (!string.IsNullOrEmpty(filterInfo.SearchTerm))
                {
                    seed = seed
                        .Where(x => x.Name.StartsWith(filterInfo.SearchTerm)
                        || x.Name.EndsWith(filterInfo.SearchTerm));
                }

                if (filterInfo.Status.HasValue)
                {
                    seed = seed.Where(x => x.Status == filterInfo.Status);
                }

                if (filterInfo.Sort?.ColName != null)
                {
                    seed = seed.OrderBy(filterInfo.Sort.ColName, filterInfo.Sort.Desc);
                }
            }

            var currentPage = filterInfo?.Page.Page ?? 0;
            seed = seed
                .Skip(DEFAULT_PAGE_SIZE * currentPage)
                .Take(DEFAULT_PAGE_SIZE);

            var list = (await seed.ToArrayAsync())
                .Select(GetDto);

            return Ok(list);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _listItemsService.DeleteItem(id);
            return Ok(true);
        }

        [HttpPatch]
        [Route("{id}/status/{status}")]
        public async Task<IActionResult> UpdateStatus(Guid id, ListItemStatus status)
        {
            var listItem = await _listItemsService.UpdateStatus(id, status);
            return Ok(GetDto(listItem));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add([FromBody] FilterInfo<ListItemDetailDto> data)
        {
            var dto = data.Dto;

            var item = new ListItem
            {
                Name = dto.Name,
                Status = ListItemStatus.Pending,
                CreatedAt = DateTimeOffset.Now,
                Data = new ListItemData
                {
                    Foo = "Foo",
                    Bar = "Bar"
                }
            };

            _context.Add(item);
            await _context.SaveChangesAsync();

            return await Index(data);
        }

        private ListItemDetailDto GetDto(ListItem listItem)
        {
            return new ListItemDetailDto
            {
                Id = listItem.Id,
                Name = listItem.Name,
                CreatedAt = listItem.CreatedAt,
                Data = listItem.Data,
                Status = listItem.Status,
                StatusText = listItem.Status.ToString(),
            };
        }
    }
}

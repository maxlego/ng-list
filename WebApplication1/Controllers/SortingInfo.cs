﻿using WebApplication1.Data.Enums;

namespace WebApplication1.Controllers
{
    public class FilterInfo
    {
        public string SearchTerm { get; set; }
        public ListItemStatus? Status { get; set; }
        public SortingInfo Sort { get; set; }
        public PagingInfo Page { get; set; }
    }

    public class FilterInfo<T> : FilterInfo
    {
        public T Dto { get; set; }
    }

    public class SortingInfo
    {
        public string ColName { get; set; }
        public bool Desc { get; set; }
    }

    public class PagingInfo
    {
        public int Page { get; set; }
    }
}
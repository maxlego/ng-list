﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;
using WebApplication1.Data.Enums;

namespace WebApplication1.Services
{
    public class ListItemsService : IListItemsService
    {
        private readonly TestContext _context;

        public ListItemsService(TestContext context)
        {
            this._context = context;
        }

        public async Task DeleteItem(Guid id)
        {
            var item = await _context.ListItems.FindAsync(id);
            await DeleteItem(item);
        }

        public async Task DeleteItem(ListItem item)
        {
            ValidateIfItemCanBeDeleted(item);
            _context.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task<ListItem> UpdateStatus(Guid id, ListItemStatus status)
        {
            var listItem = await _context.ListItems
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            // better alternative
            // var listItem = new ListItem { Id = id };
            // _context.ListItems.Attach(listItem);

            listItem.Status = status;
            await _context.SaveChangesAsync();

            return listItem;
        }

        public void ValidateIfItemCanBeDeleted(ListItem item)
        {
            var diff = DateTimeOffset.Now - item.CreatedAt;
            if (diff.TotalDays > 30)
            {
                throw new InvalidOperationException("Cannot delete items older than 30 days");
            }
        }
    }

    public interface IListItemsService
    {
        Task DeleteItem(Guid id);
        Task<ListItem> UpdateStatus(Guid id, ListItemStatus status);
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {

  private readonly _tokenString: BehaviorSubject<string>;
  public currentUser: Observable<any>;

  constructor(private _http: HttpClient) {
    this._tokenString = new BehaviorSubject<string>(localStorage.getItem("token"));
  }

  public get tokenValue(): any {
    return this._tokenString.value;
  }

  public login(username: string, password: string) {

    var host = window.location.hostname;
    return this._http.post<string>(`https://${host}:44383/api/auth`, { username, password })
      .pipe(map(res => {
        // login successful if there's a jwt token in the response
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem("token", res);
        this._tokenString.next(res);
        return res;
      }));
  }

  public logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("token");
    this._tokenString.next(null);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './_guards/auth.guard';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { ListItemsComponent } from './listItems/listItems.component';
import { ListItemComponent } from './listItems/listItem.component';
import { ListItemAddComponent } from './listItems/add.component';
import { ListItemAddButtonComponent } from './listItems/add-button.component';

import { LoginComponent } from './login/login.component';
import { AuthenticationService } from "./_services/authentication.service";
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/errors.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ListItemsComponent,
    ListItemComponent,
    LoginComponent,
    ListItemAddComponent,
    ListItemAddButtonComponent
  ],
  entryComponents: [ListItemAddComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    NgbModule,
    RouterModule.forRoot([
      {
        path: '', component: HomeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'login', component: LoginComponent
      },
      {
        path: 'listItems', component: ListItemsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'listItems/:id', component: ListItemComponent,
        canActivate: [AuthGuard]
      },
    ])
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, Inject, PACKAGE_ROOT_URL } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'list-item-add',
  templateUrl: './add.component.html'
})
export class ListItemAddComponent {

  public item = {
    name: ""
  };

  constructor(public activeModal: NgbActiveModal) { }


  public save() {
    //this._updateListItems("post", `${this.baseUrl}api/ListItems/Add`, "failed to add item", this.newItemModel);
  }

}

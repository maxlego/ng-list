import { Component, Inject, PACKAGE_ROOT_URL } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  styleUrls: ['./listItems.css'],
  templateUrl: './listItems.component.html'
})
export class ListItemsComponent {
  public items: IListItem[];
  public isDisabled = true;

  private _lastSort: ISortInfo = {} as ISortInfo;
  private _currentSort: ISortInfo = {
    colName: "Id"
  } as ISortInfo;
  private _currentPage = 0;

  public newItemModel = {
    name: ""
  };

  public searchModel = {
    term: "",  
    status: ""
  };

  public statuses = ["Pending", "Done"];

  public isAsc(colName: string) {
    return this._currentSort.colName == colName && !this._currentSort.desc;
  }

  public isDesc(colName: string) {
    return this._currentSort.colName == colName && this._currentSort.desc;
  }

  constructor(private _http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this._updateListItems("get", `${this.baseUrl}api/ListItems/`, "failed to get items");
  }

  public delete(item) {
    this._http.delete<boolean>(`${this.baseUrl}api/ListItems/${item.id}`)
      .subscribe(result => {
        if (result) {
          this._updateListItems("get", `${this.baseUrl}api/ListItems/`, "failed to get items");
        }
        else {
          console.error("failed to delete item");
        }
      }, error => console.error(error));
  }

  public setStatus(item: IListItem, status: number) {

    this._http.patch<IListItem>(`${this.baseUrl}api/ListItems/${item.id}/status/${status}`, {})
      .subscribe(result => {
        item.status = result.status;
        item.statusText = result.statusText;
      }, error => console.error(error));
  }

  public search() {
    this._updateListItems("post", `${this.baseUrl}api/ListItems`, "failed to get items");
  }

  public add() {
    this._updateListItems("post", `${this.baseUrl}api/ListItems/Add`, "failed to add item", this.newItemModel);
  }

  public next() {
    this._navigate(+1);
  }

  public prev() {
    this._navigate(-1);
  }

  private _navigate(dir: number) {
    this._currentPage += dir;
    this._updateListItems("post", `${this.baseUrl}api/ListItems`, "failed to navigate");
  } 

  public sortBy(colName: string) {

    this._currentSort.colName = colName;
    this._currentSort.desc = this._lastSort.colName == colName && !this._lastSort.desc
    this._lastSort = {... this._currentSort };

    this._updateListItems("post", `${this.baseUrl}api/ListItems`, "failed to sort items");
  }

  private async _updateListItems(method: string, url: string, errorText: string, dto: any = null) {

    var data = {
      sort: this._currentSort,
      page: {
        page: this._currentPage
      },
      searchTerm: this.searchModel.term,
      status: this.searchModel.status,
      dto: dto
    };

    this._http.request<IListItem[]>(method, url, {
      body: data,
    })
      .subscribe(
        result => this._handleUpdateListItems(result, errorText)
      );
  }

  private _handleUpdateListItems(result: IListItem[], errorText: string) {
    if (result) {
      this.items = result;
    }
    else {
      console.error(errorText);
    }
  }
}

interface IListItem {
  id: string;
  name: string;
  status: number,
  statusText: string
}

interface ISortInfo {
  colName: string,
  desc: boolean
}

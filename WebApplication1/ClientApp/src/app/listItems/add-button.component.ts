import { Component, Inject, PACKAGE_ROOT_URL } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ListItemAddComponent } from "./add.component";

@Component({
  selector: 'list-item-add-button',
  templateUrl: './add-button.component.html'
})
export class ListItemAddButtonComponent {
  
  public constructor(private _modalService: NgbModal) {

  }

  public open() {
    const modalRef = this._modalService.open(ListItemAddComponent);
  }

}

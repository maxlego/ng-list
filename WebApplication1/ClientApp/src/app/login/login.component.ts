import { Component, Inject, PACKAGE_ROOT_URL, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent {

  private _returnUrl: string;

  public model: ILoginModel = {} as ILoginModel;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _authService: AuthenticationService) {
  }

  ngOnInit() {

    // reset login status
    this._authService.logout();

    // get return url from route parameters or default to '/'
    this._returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
  }

  public login() {

    this._authService.login(this.model.username, this.model.password)
      .pipe(first())
      .subscribe(
        res => {
          if (res) {
            this._router.navigate([this._returnUrl]);
          }
        },
        error => {
          console.error(error);
        });
  }
}


interface ILoginModel {
  username: string;
  password: string;
}

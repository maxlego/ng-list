﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class ListItemStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "X",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.Sql($"UPDATE X SET [Status] = {(int)Data.Enums.ListItemStatus.Pending}");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "X");
        }
    }
}

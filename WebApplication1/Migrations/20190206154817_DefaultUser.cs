﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Security.Cryptography;
using System.Text;

namespace WebApplication1.Migrations
{
    public partial class DefaultUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var hash = SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes("admin"));
            var hashedPassword = Convert.ToBase64String(hash, Base64FormattingOptions.None);
            migrationBuilder.Sql($"INSERT INTO [Users]([Id], [Username], [Password]) VALUES(NEWID(), 'admin', '{hashedPassword}')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

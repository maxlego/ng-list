﻿using System;
using WebApplication1.Data;
using WebApplication1.Data.Enums;

namespace WebApplication1.Dto
{
    public class ListItemDetailDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public ListItemData Data { get; set; }
        public ListItemStatus Status { get; set; }
        public string StatusText { get; set; }
    }
}
